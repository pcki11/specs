#
# Be sure to run `pod lib lint NAME.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "BMHelper"
  s.version          = "0.1.6"
  s.summary          = "Helper methods for Beatman iOS apps"
  s.homepage         = "http://beatman.com"
  s.license          = 'MIT'
  s.author           = { "Gleb" => "gleb@beatman.com" }
  s.source           = { :git => "https://bitbucket.org/pcki11/bmhelper.git", :tag => s.version.to_s }

  s.platform     = :ios, '6.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes'

  s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'UIKit', 'CoreLocation', 'StoreKit'
  s.dependency 'AFNetworking'
  s.dependency 'SDURLCache'
  s.dependency 'Facebook-iOS-SDK'
end
